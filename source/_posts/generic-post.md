title: Generic Post
date: 2013-12-05 22:05:11
tags:
- test
- bummyjab
categories:
- Food
- Poison
---

### This is a generic blog post for testing newly ported themes.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
pulvinar ultricies arcu nec venenatis. Fusce quis lacus eu nulla
accumsan semper. Mauris euismod rhoncus ligula. Nunc in leo a diam
venenatis placerat vitae eget dolor. Nam malesuada nibh quis luctus
imperdiet. Aliquam ullamcorper facilisis elementum. In non est sit
amet ligula faucibus faucibus. Suspendisse blandit sem purus, a
rhoncus sem facilisis vel. Nulla vitae pulvinar odio.

Pellentesque molestie a turpis eu laoreet. Fusce fringilla dictum
libero scelerisque pharetra. Integer posuere est sit amet est dictum
imperdiet. Nulla egestas elit vel tristique posuere. Donec sapien sem,
rutrum in risus sit amet, auctor tincidunt lacus. Curabitur varius
justo libero, quis fermentum lectus hendrerit id. Sed pulvinar viverra
ultricies. Ut mattis ligula vel massa fermentum viverra.

Vestibulum fringilla rhoncus erat, ut laoreet neque tincidunt
ut. Quisque sit amet euismod arcu, ac adipiscing neque. In enim elit,
cursus in quam non, suscipit tincidunt eros. Donec consequat, metus
vitae vestibulum blandit, magna metus scelerisque nisl, vel pharetra
turpis tortor at augue. Donec porttitor condimentum risus tincidunt
imperdiet. Donec a faucibus libero. Proin sed lacus sit amet lectus
gravida viverra. Etiam velit lectus, imperdiet non ligula eu, egestas
sodales ipsum. Sed suscipit lacus vitae ullamcorper malesuada. Ut
tincidunt est risus, tincidunt commodo tellus gravida sed. Nulla nulla
urna, tristique aliquam molestie nec, vestibulum eu magna. Nulla
condimentum auctor condimentum. Aenean quis risus elit. Proin feugiat
elit in porttitor sodales. Donec elementum luctus urna, non tempus dui
malesuada in.

Vestibulum varius, elit eu consectetur convallis, lectus lorem
ultrices arcu, vel posuere sem mi vel ligula. Maecenas eu neque
nulla. Ut sed sapien fringilla, eleifend lectus et, fringilla
mauris. Integer rutrum fringilla tempor. Integer bibendum mi nibh, non
hendrerit lectus vulputate at. Phasellus eget nisl vulputate,
porttitor orci dictum, venenatis odio. Vivamus lobortis sit amet dui
eu dictum. Vivamus tincidunt turpis eros, eget rutrum mauris varius
ut. Donec sollicitudin porttitor purus non luctus. In hac habitasse
platea dictumst.

Vestibulum non sapien tincidunt, posuere magna id, consequat
diam. Curabitur tincidunt consequat nisi. Sed facilisis tristique
arcu, ultricies molestie lectus semper quis. Nam cursus quis diam
convallis eleifend. Mauris fringilla sodales imperdiet. Vestibulum
eleifend, turpis et congue condimentum, odio leo aliquet velit, auctor
suscipit quam sem cursus neque. Nam commodo in nunc id tincidunt. Nam
quam turpis, interdum eget luctus sed, aliquam et diam. Phasellus
placerat vel urna non lacinia. Phasellus ultrices varius
fringilla. Duis aliquet turpis vel euismod consectetur. Praesent
dapibus, elit nec dignissim suscipit, nisl dolor eleifend est, in
aliquet ligula dui nec elit. Cras sagittis lacus eget arcu aliquet
dapibus. Donec eu massa euismod ante tempus iaculis. Nam molestie
dapibus tristique.

    #!/bin/bash
    # vimntu - quick install of plugins and distributions i find useful
    # author: Adam Stokes <adam.stokes@ubuntu.com>
    ###############################################################################
    
    # set -x
    
    VIMRCPATH=${HOME}/.vimrc.after
    GVIMRCPATH=${HOME}/.gvimrc.after
    JANUSPATH=${HOME}/.janus
    
    if [[ ! -d ${JANUSPATH} ]]; then
      mkdir -p ${JANUSPATH}
    fi
    
    # VIMRC config
    read -r -d '' VIMRCAFTER <<'EOT'
    color jellybeans
    set textwidth=79
    set formatoptions=qrn1
    if exists('+colorcolumn')
      set colorcolumn=80
    endif
    set list
    set listchars=tab:.\ ,trail:.,extends:#,nbsp:.
    set numberwidth=5
    set cursorline
    set cursorcolumn
    EOT
    
    # GVIMRC config
    read -r -d '' GVIMRCAFTER <<'EOT'
    color jellybeans
    if has("gui_running")
      set guifont=Monospace\ 10
      set list
      set listchars=tab:▸\ ,eol:¬,extends:#,nbsp:.,trail:.
      set guioptions-=r
      set go-=L
      set go-=T
    endif
    EOT
    
    install_pkgs() {
      packages=(vim \
        vim-gtk \
        git \
        ctags \
        ruby1.9.1 \
        rake
      )
      # only install packages we need
      for i in "${packages[@]}"
      do
        dpkg -l ${i} &> /dev/null
        RET=$?
        if [[ "$RET" -eq "1" ]]; then
          sudo apt-get -y -qq --force-yes ${i}
        fi
      done
    }
    
    install_janus() {
      echo "Installing janus"
      ( if [ -x /usr/bin/curl ]; then
          curl -Lo- https://bit.ly/janus-bootstrap | bash
        elif [ -x /usr/bin/wget ]; then
          wget --no-check-certificate https://bit.ly/janus-bootstrap -O - | bash
        else
          echo "No curl/wget found, install with apt-get install curl"
          return 1
        fi ) &> /dev/null
      echo "Installing custom vim configs"
      if [ ! -f ${VIMRCPATH} ]; then
        echo "${VIMRCAFTER}" > ${VIMRCPATH}
      fi
      if [ ! -f ${GVIMRCPATH} ]; then
        echo "${GVIMRCAFTER}" > ${GVIMRCPATH}
      fi
    }
    
    install_git_pkgs() {
      packages=(
        git://github.com/antono/html.vim \
        git://github.com/bling/vim-airline \
        git://github.com/klen/python-mode \
        git://github.com/nanotech/jellybeans.vim \
        git://github.com/othree/html5.vim \
        git://github.com/rodjek/vim-puppet \
        git://github.com/sickill/vim-pasta \
        git://github.com/tpope/vim-bundler \
        git://github.com/tpope/vim-endwise \
        git://github.com/tpope/vim-eunuch \
        git://github.com/tpope/vim-fugitive \
        git://github.com/tpope/vim-haml \
        git://github.com/tpope/vim-rails \
        git://github.com/tpope/vim-scriptease \
        git://github.com/tpope/vim-sensible \
        git://github.com/tpope/vim-surround \
        git://github.com/vimoutliner/vimoutliner \
        git://github.com/vim-perl/vim-perl \
        git://github.com/vim-ruby/vim-ruby \
        git://github.com/Yggdroot/indentLine \
        git://github.com/yko/mojo.vim \
        git://github.com/digitaltoad/vim-jade \
        git@github.com:wavded/vim-stylus
      )
      for i in "${packages[@]}"
      do
        echo "Installing plugin: $(basename ${i})"
        # only if url ends with .git
        # _dir="${i##*/}"
        if [ -d ${i} ]; then
          cd ${JANUSPATH}/${i} && git pull -q
        else
          cd ${JANUSPATH} && git clone -q ${i}
        fi
      done
    }
    
    install_pkgs
    install_janus
    install_git_pkgs
    
    exit $?

